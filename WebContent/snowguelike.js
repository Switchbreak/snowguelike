var createScene = function(engine, canvas){
    // create a basic BJS Scene object
    var scene = new BABYLON.Scene(engine);

    // create a FreeCamera, and set its position to (x:0, y:5, z:-10)
    var camera = new BABYLON.FreeCamera('camera1', new BABYLON.Vector3(0, 5,-10), scene);
    //var camera = new BABYLON.ArcFollowCamera('camera1', alpha, beta, radius, target, scene)

    // target the camera to scene origin
    camera.setTarget(BABYLON.Vector3.Zero());
    
    // attach the camera to the canvas
    camera.attachControl(canvas, false);

    // create a basic light, aiming 0,1,0 - meaning, to the sky
    var light = new BABYLON.DirectionalLight('light1', new BABYLON.Vector3(0,-5,0), scene);
	var lightImpostor =  BABYLON.Mesh.CreateSphere("sphere1", 16, 1, scene);
	var lightImpostorMat = new BABYLON.StandardMaterial("mat", scene);
	lightImpostor.material = lightImpostorMat;
	lightImpostorMat.emissiveColor = BABYLON.Color3.Yellow();
	lightImpostorMat.linkEmissiveWithDiffuse = true;
	
	lightImpostor.parent = light;

    // create a built-in "sphere" shape; its constructor takes 5 params: name, width, depth, subdivisions, scene
    var sphere = BABYLON.Mesh.CreateSphere('sphere1', 16, 2, scene);

    // move the sphere upward 1/2 of its height
    sphere.position.y = 2;

    // create a built-in "ground" shape; its constructor takes the same 5 params as the sphere's one
    var ground = BABYLON.Mesh.CreateGround('ground1', 6, 6, 2, scene);
    
    var shadowGenerator = new BABYLON.ShadowGenerator(1024, light);
    shadowGenerator.getShadowMap().renderList.push(sphere);
    ground.receiveShadows = true;

    // return the created scene
    return scene;
}
